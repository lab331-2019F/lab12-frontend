import { Component } from '@angular/core';
import Student from '../../entity/student';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import { Router } from '@angular/router';
import{ environment }from 'src/environments/environment';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FileUploadService } from 'src/app/service/file-upload.service';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent {
  uploadEndPoint : string;
  uploadedUrl: string;
  progress: number;  
  ngOnInit() : void{
    this.uploadEndPoint=environment.uploadApi;
  }
  students: Student[];
  form = this.fb.group({
    id: [''],
    studentId: [null, Validators.compose([Validators.required, Validators.maxLength(13)])],
    name: [null, Validators.required],
    surname: [null, Validators.required],
    gpa: [''],
    image: [''],
    featured: [''],
    penAmount: [null, Validators.compose([Validators.required,Validators.pattern('[0-9]+')])],
    description: ['']
  });

  validation_messages = {
    'studentId': [
      { type: 'required', message: 'student id is required' },
      { type: 'maxlength', message: 'student id is too long' }
    ],
    'name': [
      { type: 'required', message: 'the name is required' }
    ],
    'surname': [
      { type: 'required', message: 'the surname is required' }
    ]
    ,
    'penAmount': [
      { type: 'required', message: 'the penAmount is required' },
      { type: 'pattern', message: 'please enter number'}
    ]
    ,
    'image': []
    ,
    'description': []
  };



  get diagnostic() {
    return JSON.stringify(this.form.value);
  };

  upQuantity(student: Student) {
    this.form.patchValue({
      penAmount: +this.form.value['penAmount'] + 1
    });

  }

  downQuantity(student: Student) {
    if (+this.form.value['penAmount'] > 0) {
      this.form.patchValue({
        penAmount: +this.form.value['penAmount'] - 1
      });
    }
  }


  submit() {

    this.studentService.saveStudent(this.form.value)
      .subscribe((student) => {
        this.router.navigate(['/detail/', student.id]);
      }, (error) => {
        alert('could not save value');
      });
  }
  onUploadClicked(files?:FileList){
    console.log(typeof(files));
    console.log(files.item(0));
    const uploadFile=files.item(0);
    this.progress=0;
    this.fileUploadService.uploadFile(uploadFile)
    .subscribe((event: HttpEvent<any>)=>{
      switch(event.type){
        case HttpEventType.Sent:
          console.log('Request has been made!');
          break;
          case  HttpEventType.ResponseHeader:
            console.log('Request header has been received!');
            break;
            case HttpEventType.UploadProgress:
              this.progress=Math.round(event.loaded/event.total*100);
              console.log('Upload! ${this.progress}%');
              break;
              case HttpEventType.Response:
                console.log('User successfully created!',event.body);
                this.uploadedUrl=event.body;
                this.form.patchValue({
                  image:this.uploadedUrl
                });

                setTimeout(()=>{
                  this.progress=0;
                },1500);
      }
    });
  }

  onSelectedFilesChanged(files?:FileList){
    
  }


  constructor(private fb: FormBuilder, private fileUploadService: FileUploadService,private studentService:StudentService,private router: Router) {

  }

}
